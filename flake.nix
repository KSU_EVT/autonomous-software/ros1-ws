{
  inputs = {
    ros.url = "github:lopsided98/nix-ros-overlay";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, ros, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import (ros.inputs.nixpkgs) {
            system = "${system}";
            overlays = [
	            ros.overlay
	          ];
          };
          noetic = pkgs.rosPackages.noetic;
        in
        {
          devShell = with pkgs; with noetic; mkShell {
            nativeBuildInputs = [
              cmake
              vcstool
              python3

              rosbash
              roslint
              catkin
              python3Packages.catkin-tools
              rostopic
              rosbag
              roslaunch
              rospack
              turtlesim

              # I don't feel like doing the whole generated.nix stuff, this is not going to be used much
              roscpp
              pcl-ros
              nav-msgs
              pcl
              pcl-conversions
              visualization-msgs
              opencv
              boost
            ];
            ROS_PYTHON_VERSION = "3";
            ROSBASH_PATH = "${rosbash}/share/rosbash";

            ROS_HOSTNAME = "localhost";
            ROS_MASTER_URI = "http://localhost:11311";
          };
        }
      );
}

